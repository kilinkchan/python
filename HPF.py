# -*- coding: utf-8 -*-
"""
Created on Wed Aug 15 17:08:20 2018

@author: kilink.chan
"""

import cv2
import numpy as np
from scipy import ndimage

ker_3x3 = np.array([[-1, -1, -1], 
                    [-1, 8, -1],
                    [-1, -1, -1]])

ker_5x5 = np.array([[-1, -1, -1, -1, -1],
                    [-1, -1, 2, -1, -1],
                    [-1, 2, 4, 2, -1],
                    [-1, -1, 2, -1, -1],
                    [-1, -1, -1, -1, -1]])

img = cv2.imread("C:\\Users\\kilink.chan\\Desktop\\python\\lenna.jpg", 0)

k3 = ndimage.convolve(img, ker_3x3)
k5 = ndimage.convolve(img, ker_5x5)

blurred = cv2.GaussianBlur(img, (11,11), 0)
g_hpf = img - blurred

cv2.imshow("3x3", k3)
cv2.imshow("5x5", k5)
cv2.imshow("g_hpf", g_hpf)


img2 = cv2.imread("C:\\Users\\kilink.chan\\Desktop\\python\\lenna.jpg")
cv2.imshow("lenna", img2)
cv2.imwrite("canny.jpg", cv2.Canny(img2, 200, 300))


cv2.waitKey()
cv2.destroyAllWindows()