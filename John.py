# -*- coding: utf-8 -*-
"""
Created on Tue Aug 21 14:06:27 2018

@author: kilink.chan
"""

from People import People

class John(People):
    def __init__(self, name, x, y):
        super(John, self).__init__(name)
        self.x = x
        self.y = y
    def setJobName(self, work):
        self.work = work
    def getJobName(self):
        return str(self.work)+str(self.name)
    def __lt__(self, other):
        return self.x < other.x and self.y < other.y