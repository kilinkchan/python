class Point:
   def __init__(self, x):
        self.x = x
        
   def __gt__(self, that):
        return self.x >= that.x
        
   def __eq__(self, that):
        return self.x == that.x

p1 = Point(3)
p2 = Point(2)
print(p1 > p2)    # True
print(p1 is p2)    # False